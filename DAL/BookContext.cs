﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class BookContext : DbContext
    {
        public BookContext() : base("BookStore")
        {

        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            builder.Entity<Book>().ToTable("Books");
            builder.Entity<Book>().HasKey(x => x.Id);
            builder.Entity<Book>().Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            builder.Entity<Book>().HasMany(x => x.Authors).WithMany(x => x.Books)
                .Map(x =>
                {
                    x.ToTable("BookAuthor");
                    x.MapLeftKey("Book_Id");
                    x.MapRightKey("Author_Id");
                });

            builder.Entity<Author>().ToTable("Authors")
                .HasKey(x => x.Id);

            builder.Entity<Author>().Property(x => x.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            builder.Entity<BookAuthor>().HasKey(x => x.Author_Id).Property(x=> x.Author_Id).IsRequired();
            builder.Entity<BookAuthor>().HasKey(x => x.Book_Id).Property(x=> x.Book_Id).IsRequired();

            base.OnModelCreating(builder);
        }

        public DbSet<Book> Books { get; set; }
        public DbSet<Author> Authors { get; set; }
        public DbSet<BookAuthor> BookAuthor { get; set; }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
