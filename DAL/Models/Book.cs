﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Models
{
    public class Book : IEntity
    {
        public Book()
        {
            Authors = new List<Author>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? CreationDate { get; set; }
        public int Rate { get; set; }
        public int PageCount { get; set; }

        public virtual ICollection<Author> Authors { get; set; }
        //public ICollection<BookAuthor> BookAuthor { get; set; }
    }
}
