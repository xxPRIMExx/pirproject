﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class BaseRepository<T> where T : class
    {
        BookContext _context;
        public DbSet<T> Local => _context.Set<T>();

        public BaseRepository(BookContext context)
        {
            _context = context;
        }
    }
}
