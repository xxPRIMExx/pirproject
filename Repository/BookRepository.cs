﻿using DAL;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using EntityFramework.Extensions;
using System;

namespace Repository
{
    public class BookRepository: BaseRepository<Book>
    {
        public BookRepository(BookContext context) : base(context) 
        {

        }

    }
}
