﻿using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repository
{
    public class AuthorRepository: BaseRepository<Author>
    {
        public AuthorRepository(BookContext context): base(context)
        {

        }
    }
}
