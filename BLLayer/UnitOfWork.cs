﻿using DAL;
using Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLLayer
{
    public class UnitOfWork
    {
        public readonly BookContext Context;
        object lockObj = new object();

        public UnitOfWork()
        {
            Context = new BookContext();
        }

        #region RepositoriesField
        private BookRepository _bookRepository;
        private AuthorRepository _authorRepository;
        #endregion

        #region Repositories prop
        public BookRepository BookRepository
        {
            get
            {
                if (_bookRepository == null)
                    _bookRepository = new BookRepository(Context);
                return _bookRepository;
            }
        }
        public AuthorRepository AuthorRepository
        {
            get
            {
                if (_authorRepository == null)
                    _authorRepository = new AuthorRepository(Context);
                return _authorRepository;
            }
        }
        #endregion

        public void SaveChanges()
        {
            lock (lockObj)
                Context.SaveChanges();
        }
    }
}
