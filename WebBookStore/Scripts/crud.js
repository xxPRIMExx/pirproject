"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var BookModel_1 = require("./models/BookModel");
var AuthorModel_1 = require("./models/AuthorModel");
$('input[name=btnInsert]').click(function (e) {
    e.preventDefault();
    var book = new BookModel_1.Book;
    book.Name = $('input[name=Name]').val();
    book.Rate = $('input[name=Rate]').val();
    book.PageCount = $('input[name=PageCount]').val();
    var selected = $('select[name=Authors]').val();
    console.log(selected);
    for (var i = 0; i < selected.length; i++) {
        var author = new AuthorModel_1.Author;
        author.Id = selected[i];
        book.Authors.push(author);
    }
    AddOrUpdateBook(book);
});
$('input[name=btnDelete]').click(function (e) {
    e.preventDefault();
    var bookId = parseInt($(this).attr("id"));
    $(this).closest("tr").remove();
    RemoveBook(bookId);
});
function AddOrUpdateBook(book) {
    $.ajax({
        type: 'POST',
        url: '/api/Book',
        data: JSON.stringify(book),
        contentType: "application/json; charset=utf-8",
        dataType: 'json',
        error: function () { alert('Error'); }
    });
}
function RemoveBook(id) {
    $.ajax({
        type: 'DELETE',
        url: 'api/Book/' + id,
        data: id,
        error: function () { alert('Error'); }
    });
}
