﻿/// <reference path="typings/jquery/jquery.d.ts" />
/// <reference path="./models/AuthorModel.ts" />
/// <reference path="./models/BookModel.ts" />

import { Book } from "./models/BookModel";
import { Author } from "./models/AuthorModel";
$(document).ready(function () {
    $('input[name=btnInsert]').click(function (e) {
        e.preventDefault();

        let book: Book = new Book;
        book.Name = $('input[name=Name]').val();
        book.Rate = $('input[name=Rate]').val();
        book.PageCount = $('input[name=PageCount]').val();

        let selected = $('select[name=Authors]').val();
        console.log(selected);

        for (let i: number = 0; i < selected.length; i++) {
            let author: Author = new Author;
            author.Id = selected[i];
            book.Authors.push(author);
        }

        AddOrUpdateBook(book);
    });

    function ButtonInsertOnClick() {

    }

    $('input[name=btnDelete]').click(function (e) {
        e.preventDefault();
        let bookId: number = parseInt($(this).attr("id"));
        $(this).closest("tr").remove();
        RemoveBook(bookId);
    });

    function AddOrUpdateBook(book: Book): void {
        $.ajax({
            type: 'POST',
            url: '/api/Book',
            data: JSON.stringify(book),
            contentType: "application/json; charset=utf-8",
            dataType: 'json',
            error: function () { alert('Error'); }
        });
    }


    function RemoveBook(id: number) {
        $.ajax({
            type: 'DELETE',
            url: 'api/Book/' + id,
            data: id,
            error: function () { alert('Error'); }
        });
    }
});


//export class Author {
//    Id: number;
//    FirstName: string;
//    LastName: string;
//    Books: Array<Book>;
//}

//export class Book {
//    Id: number;
//    Name: string;
//    CreationDate: Date;
//    Rate: number;
//    PageCount: number;
//    Authors: Array<Author>;
//}