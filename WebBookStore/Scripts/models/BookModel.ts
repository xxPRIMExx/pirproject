﻿/// <reference path="./AuthorModel.ts" />
import { Author } from "./AuthorModel.js";

export class Book {
    Id: number;
    Name: string;
    CreationDate: Date;
    Rate: number;
    PageCount: number;
    Authors: Array<Author>;
}