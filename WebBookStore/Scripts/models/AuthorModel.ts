﻿/// <reference path="./BookModel.ts" />
import { Book } from "./BookModel.js";

export class Author {
    Id: number;
    FirstName: string;
    LastName: string;
    Books: Array<Book>;
}