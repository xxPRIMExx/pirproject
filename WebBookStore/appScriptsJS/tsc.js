define("models/BookModel", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Book = (function () {
        function Book() {
        }
        return Book;
    }());
    exports.Book = Book;
});
define("models/AuthorModel", ["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Author = (function () {
        function Author() {
        }
        return Author;
    }());
    exports.Author = Author;
});
define("crud", ["require", "exports", "models/BookModel", "models/AuthorModel"], function (require, exports, BookModel_1, AuthorModel_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    $(document).ready(function () {
        $('input[name=btnInsert]').click(function (e) {
            e.preventDefault();
            var book = new BookModel_1.Book;
            book.Name = $('input[name=Name]').val();
            book.Rate = $('input[name=Rate]').val();
            book.PageCount = $('input[name=PageCount]').val();
            var selected = $('select[name=Authors]').val();
            console.log(selected);
            for (var i = 0; i < selected.length; i++) {
                var author = new AuthorModel_1.Author;
                author.Id = selected[i];
                book.Authors.push(author);
            }
            AddOrUpdateBook(book);
        });
        function ButtonInsertOnClick() {
        }
        $('input[name=btnDelete]').click(function (e) {
            e.preventDefault();
            var bookId = parseInt($(this).attr("id"));
            $(this).closest("tr").remove();
            RemoveBook(bookId);
        });
        function AddOrUpdateBook(book) {
            $.ajax({
                type: 'POST',
                url: '/api/Book',
                data: JSON.stringify(book),
                contentType: "application/json; charset=utf-8",
                dataType: 'json',
                error: function () { alert('Error'); }
            });
        }
        function RemoveBook(id) {
            $.ajax({
                type: 'DELETE',
                url: 'api/Book/' + id,
                data: id,
                error: function () { alert('Error'); }
            });
        }
    });
});
