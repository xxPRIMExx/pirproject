﻿using BLLayer;
using DAL;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebBookStore.Controllers
{
    public class BookController : ApiController
    {
        private readonly UnitOfWork _unitOfWork;
        public BookController(UnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }
        // GET: api/Book
        public IEnumerable<Book> Get()
        {
            return _unitOfWork.BookRepository.GetList();
        }

        // GET: api/Book/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Book
        public void Post([FromBody]Book book)
        {
            int[] authorsId = book.Authors.Select(x => x.Id).ToArray();
            IEnumerable<Author> authors = _unitOfWork.Context.Authors.Where(x => authorsId.Contains(x.Id));

            book.Authors = authors.ToList();
            _unitOfWork.BookRepository.AddOrUpdate(book);

            _unitOfWork.SaveChanges();
        }

        // PUT: api/Book/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Book/5
        public void Delete(int id)
        {
            _unitOfWork.BookRepository.Delete(id);
            _unitOfWork.SaveChanges();


        }
    }
}
