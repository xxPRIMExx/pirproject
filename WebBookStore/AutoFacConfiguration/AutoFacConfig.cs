﻿using Autofac;
using Autofac.Integration.Mvc;
using Autofac.Integration.WebApi;
using BLLayer;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace WebBookStore.AutoFacConfiguration
{
    public class AutoFacConfig
    {
        public static void ConfigureContainer()
        {
            // получаем экземпляр контейнера
            var builder = new ContainerBuilder();

            // регистрируем контроллер в текущей сборке
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // регистрируем споставление типов
            builder.RegisterType<UnitOfWork>().SingleInstance();

            // создаем новый контейнер с теми зависимостями, которые определены выше
            var сontainer = builder.Build();

            // установка сопоставителя зависимостей
            DependencyResolver.SetResolver(new AutofacDependencyResolver(сontainer));



            var apiBuilder = new ContainerBuilder();

            // Get your HttpConfiguration.
            var config = GlobalConfiguration.Configuration;

            // Register your Web API controllers.
            apiBuilder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            // OPTIONAL: Register the Autofac filter provider.
            apiBuilder.RegisterWebApiFilterProvider(config);

            // OPTIONAL: Register the Autofac model binder provider.
            apiBuilder.RegisterWebApiModelBinderProvider();
            apiBuilder.RegisterType<UnitOfWork>().SingleInstance();

            // Set the dependency resolver to be Autofac.
            var apiContainer = apiBuilder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(apiContainer);
        }
    }
}